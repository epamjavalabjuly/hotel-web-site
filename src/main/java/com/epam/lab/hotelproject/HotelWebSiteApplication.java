package com.epam.lab.hotelproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotelWebSiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelWebSiteApplication.class, args);
	}
}
